import os, sys
import subprocess



configpath = os.environ['APPDATA']+"\PowerSaver\config.ini"
configtool = os.environ['APPDATA']+"\PowerSaver\config.exe"
startparams = sys.argv[1:]


if(len(startparams) > 0):
    startparams = startparams[0]
else:
    startparams = "/c"

if(os.path.exists(configpath)==False):
    print "No Config File Found starting Config Mode"
    startparams = "/c"



if(startparams == "/s"):
    print "Mode: /s Presentation starter"
    #READ CONFIG
    cfile = open(configpath, "r")
    config = cfile.readline().split(";")
    cfile.close()
    print config

    ## INIT VARS
    pptfile = config[0]
    if(config[1] == "1"):
        customviewer = config[2]
    if(config[3] == "1"):
        fullscreen = 1
    else:
        fullscreen = 0

    ## Viewerlist    
    ppvlist = []
    ppvlist.append("C:\Program Files (x86)\Microsoft Office\Office14\PPTVIEW.EXE")
    ppvlist.append("C:\Program Files\Microsoft Office\Office14\PPTVIEW.EXE")
    ppvlist.append("C:\Programme\Microsoft Office\Office14\PPTVIEW.EXE")


    ## Check for PPT-File
    if(os.path.exists(pptfile)):
        print "Powerpoint File found!"
        foundppt = 1
    else:
        print "Powerpoint File not found!"
        foundppt = 0

    ## Check for Custom Viewer
    if(config[1] == "1"):
        foundppv = 1
        path = customviewer
        print "Using custom PPT Viewer"
    else:
        foundppv = 0
        for item in ppvlist:
            if(os.path.exists(item)):
                foundppv = 1
                path = item
                print "Powerpoint Viewer found ("+path+")"

    ## Presentation Start    
    if(foundppv):
        if(fullscreen == 1):
            command = path+" /s /f "+pptfile
        else:
            command = path+" /s "+pptfile
        if(foundppv == 1 and foundppt == 1):    
            print "Starting Presentation"    
            subprocess.call(command, shell=False)
elif(startparams.startswith("/p")):
    pass
else:
    print "Mode: not /s starting config"
    subprocess.call(configtool, shell=False)
    
