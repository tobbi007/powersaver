from Tkinter import *
import tkFileDialog
import sys, os

configpath = os.environ['APPDATA']+"\PowerSaver\config.ini"


##Funcs
def getPPTFile():
     filename = tkFileDialog.askopenfilename(parent=root,initialdir="/",title='Please select a PPT-File')
     Entry_PPTpath.delete(0, END)
     Entry_PPTpath.insert(0, filename)

def getCustomViewer():
     filename = tkFileDialog.askopenfilename(parent=root,initialdir="/",title='Please select a PPT Viewer')
     Entry_cview.delete(0, END)
     Entry_cview.insert(0, filename)

def insertConfig():
    Entry_PPTpath.delete(0, END)
    Entry_PPTpath.insert(0, config[0])

    Entry_cview.delete(0, END)
    Entry_cview.insert(0, config[2])

    if(config[1] == "1"):
        customviewer.set(1)
    if(config[3] == "1"):
        fullscreen.set(1)

def saveConfigFile():
    configtext = Entry_PPTpath.get()+";"+str(customviewer.get())+";"+Entry_cview.get()+";"+str(fullscreen.get())
    print configtext
    cfile = open(configpath, "w")
    cfile.write(configtext)
    cfile.close()
    exitProgram()
    
def exitProgram():
    root.destroy()
    sys.exit()
    
        
root = Tk()
root.title("PowerSaver Config")
root.geometry("420x180+100+100")



## GUI
Label_Head = Label(root, text="PowerSaver Config Tool", font=("Arial", 16),justify=LEFT)
Label_Head.place(x=10,y=10,width=240,height=25)

## pptfile
Label_PPTpath = Label(root, text="PPT-Presentation")
Label_PPTpath.place(x=10,y=50,width=100,height=20)

Entry_PPTpath = Entry(root)
Entry_PPTpath.place(x=120,y=50,width=200,height=20)

Button_PPTpath = Button(root, text="...",command=getPPTFile)
Button_PPTpath.place(x=330,y=50,width=50,height=20)

## Custom Viewer
Label_cview = Label(root, text="Custom Viewer")
Label_cview.place(x=10,y=80,width=100,height=20)

Entry_cview = Entry(root)
Entry_cview.place(x=120,y=80,width=200,height=20)

Button_cview = Button(root, text="...",command=getCustomViewer)
Button_cview.place(x=330,y=80,width=50,height=20)

customviewer = IntVar()

Checkbox_cview = Checkbutton(root, text="Use custom Viewer", variable=customviewer)
Checkbox_cview.place(x=10,y=110,width=150,height=20)

##Fullscreen
fullscreen = IntVar()

Checkbox_fscreen = Checkbutton(root, text="Use Fullscreen Presentation", variable=fullscreen)
Checkbox_fscreen.place(x=200,y=110,width=170,height=20)


##Buttons

Button_save = Button(root, text="Save",command=saveConfigFile)
Button_save.place(x=10,y=140,width=100,height=20)

Button_cancel = Button(root, text="Cancel",command=exitProgram)
Button_cancel.place(x=310,y=140,width=100,height=20)


if(os.path.exists(configpath)):
     ##READ CONFIG
     cfile = open(configpath, "r")
     config = cfile.readline().split(";")
     cfile.close()
     print config
     insertConfig()
     
root.mainloop()
