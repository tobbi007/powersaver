Powersaver
==========

This little Python (2.7) program can be used to start a .ppt(x) file as a screensaver on windows.

It currently requires:
- Microsoft Powerpoint Viewer (Office 14 Version) 
- You can download it for free here: http://bit.ly/V2Art9
- A .ppt(x) file

You can find compiled versions under /build or the Downloads page
They all have been compiled by py2exe for python 2.7.



Installation
============
You can try using the install scripts in the build folders!

OR

1.
Copy powersaver.exe and w9xpopen.exe to:

  C:\Windows\system32 on x86 System
or
  C:\Windows\syswow64 on x64 System
  
2.
Copy all Files from config folder to
%appdata%\PowerSaver

3.
Setup powersaver as your windows screensaver. Configure it!


There will be an Installation Script later.



Compile
============

In the sourcefolder there are 2 scripts.

1. compile_config.py
2. compile_saver.py

call them like this:

python compile_config.py [or compile_saver.py] py2exe

the compiles will be in the dist folder.
It only works if you have py2exe installed!!

